<?php

namespace App\DataFixtures;

use App\Entity\Job;
use App\Entity\JobCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class JobFixtures extends Fixture
{
    private array $job=[
        'Chef de Cuisine',
        'Sous-Chef',
        'Serveur/Serveuse',
        'Plongeur/Plongeuse',
        'Barman/Barmaid',
        'Maître d\'Hôtel',
        'Commis de Cuisine',
        'Pâtissier/Pâtissière',
        'Parking Boy/Girl',
    ];


    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $categories= $manager->getRepository(JobCategory::class)->findAll();
        foreach($this->job as $job) {
            $jobs = new Job();
            $jobs->setName($job)
                ->setSmallDescription($faker->sentence(3, true))
                ->setFullDescription($faker->paragraph(5, true))
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable())
                ->setPublished($faker->boolean(90))
                ->setImage('0'.$faker->numberBetween(1, 9).'.jpg')
                ->setCategory($categories[array_rand($categories)]);
            $manager->persist($jobs);
        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
