<?php

namespace App\DataFixtures;
use Faker\Factory;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    private object $hasher;
    public function __construct(UserPasswordHasherInterface $hasher) {
        $this->hasher = $hasher;
    }
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        for($i = 1; $i <= 10; $i++) {
            $user = new User();
            $user->setEmail($faker->email);
            $user->setPassword($this->hasher->hashPassword($user, 'password'));
            $user->setName($faker->name);
            $user->setRoles(['ROLE_USER']);
            $manager->persist($user);
        }
        $admin = new User();
        $admin->setEmail('admin@augredevosenvies6811.com');
        $admin->setPassword($this->hasher->hashPassword($admin, '123456'));
        $admin->setName('Admin');
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);

        $manager->flush();

    }
}
