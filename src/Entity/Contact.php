<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Contact
{

    #[Assert\NotBlank(message: 'Ce champ ne peut être vide')]
    private string $name;
    #[Assert\NotBlank(message: 'Ce champ ne peut être vide')]
    private string $email;
    #[Assert\NotBlank(message: 'Ce champ ne peut être vide')]
    private string $subject;
    #[Assert\NotBlank(message: 'Ce champ ne peut être vide')]
    private string $message;






    /**
     * Get the value of name
     */
    public function getName(): string
    {
        return $this->name;
    }
    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name): static
    {
        $this->name = $name;
        return $this;
    }
    /**
     * Get the value of email
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email): static
    {
        $this->email = $email;
        return $this;
    }
    /**
     * Get the value of subject
     */
    public function getSubject(): string
    {
            return $this->subject;
        }
        /**
         * Set the value of subject
         *
         * @return  self
         */
    public function setSubject($subject): static
    {
            $this->subject = $subject;
            return $this;
        }
        /**
         * Get the value of message
         */
    public function getMessage(): string
    {
            return $this->message;
        }
        /**
         * Set the value of message
         *
         * @return  self
         */
    public function setMessage($message): static
    {
            $this->message = $message;
            return $this;
        }



}
