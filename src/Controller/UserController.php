<?php

namespace App\Controller;

use App\Form\EditProfileType;
use App\Repository\BusinessRepository;
use App\Repository\EventRepository;
use App\Repository\WeddingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Doctrine\ORM\EntityManagerInterface as EntityManager;
use App\Entity\User;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class UserController extends AbstractController
{
    #[Route('/user', name: 'app_user')]
    #[IsGranted('ROLE_USER')]
    public function index(): Response
    {
        return $this->render('user/user_home.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    #[Route(path: '/logout', name: 'app_logout', methods: ['GET'])]
    public function logout(): void
    {

    }
    #[Route(path: '/user/event_user', name: 'app_event_user')]
    #[IsGranted('ROLE_USER')]
    public function eventUser(WeddingRepository $weddingRepository, EventRepository $eventRepository, BusinessRepository $businessRepository): Response
    {
        $event = $eventRepository->findAll();
        $wedding = $weddingRepository->findAll();
        $business = $businessRepository->findAll();


        return $this->render('user/event_user.html.twig', [
            'wedding' => $wedding,
            'event' => $event,
            'business'=> $business,
        ]);

    }
    #[Route('/user/edit-profile', name: 'app_edit_profile')]
    #[IsGranted('ROLE_USER')]
    public function editProfil(Request $request, EntityManagerInterface $em, UserPasswordHasherInterface $passwordHasher): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(EditProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $plainPassword = $form->get('plainPassword')->getData();
            if ($plainPassword) {
                $hashedPassword = $passwordHasher->hashPassword($user, $plainPassword);
                $user->setPassword($hashedPassword);
            }
            $em->flush();

            $this->addFlash('success', 'Votre profil a été mis à jour avec succès.');

            return $this->redirectToRoute('app_user');
        }

        return $this->render('user/edit_profile.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    #[Route('/user/delete-account', name: 'app_delete_account', methods: ['POST'])]
    #[IsGranted('ROLE_USER')]
    public function deleteAccount(Request $request, EntityManagerInterface $em): Response
    {
        $user = $this->getUser();
        if (!$user) {
            throw new AccessDeniedException('Vous devez être connecté pour supprimer votre compte.');
        }

        if ($this->isCsrfTokenValid('delete_account', $request->request->get('_token'))) {
            $em->remove($user);
            $em->flush();

            $this->addFlash('success', 'Votre compte a été supprimé avec succès.');

            return $this->redirectToRoute('app_logout');
        }

        $this->addFlash('error', 'Token CSRF invalide.');
        return $this->redirectToRoute('app_edit_profile');
    }
}
