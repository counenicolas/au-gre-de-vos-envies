<?php

namespace App\Controller;

use App\Entity\Business;
use App\Entity\Event;
use App\Entity\Wedding;
use App\Form\BusinessType;
use App\Form\EventType;
use App\Form\WeddingType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class EventController extends AbstractController
{
    #[Route('/event', name: 'event')]
    public function event(): Response
    {
        return $this->render('event/event.html.twig');
    }

    #[Route('/event/business', name: 'event_business')]
    public function business(Request $request, EntityManagerInterface $em  ): Response
    {
        $business = new Business();
        $form = $this->createForm(BusinessType::class, $business);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($business);
            $em->flush();
            return $this->redirectToRoute('event');
        }
        return $this->render('event/business.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    #[Route('/event/wedding', name: 'event_wedding')]
    public function wedding(Request $request, EntityManagerInterface $em): Response
    {
        $wedding = new Wedding();
        $form = $this->createForm(WeddingType::class, $wedding);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($wedding);
            $em->flush();
            return $this->redirectToRoute('event');
        }
        return $this->render('event/wedding.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    #[Route('/event/events', name: 'event_events')]
    public function events(Request $request, EntityManagerInterface $em): Response
    {
        $event = new Event();
        $form = $this->createForm(EventType::class, $event);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($event);
            $em->flush();
            return $this->redirectToRoute('event');
        }
        return $this->render('event/events.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}
