<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class PageController extends AbstractController
{
    #[Route('/gallery', name: 'gallery')]
    public function gallery(): Response
    {
        return $this->render('page/gallery.html.twig', [
            'controller_name' => 'PageController',
        ]);
    }

}
