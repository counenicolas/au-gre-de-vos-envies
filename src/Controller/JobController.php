<?php

namespace App\Controller;

use App\Entity\Job;
use App\Repository\JobCategoryRepository;
use App\Repository\JobRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class JobController extends AbstractController
{
    #[Route('/jobs', name: 'jobs')]
    public function job(JobRepository $jobRepository, JobCategoryRepository $jobCategoryRepository): Response
    {
        $jobs = $jobRepository->findAll();
        $categories = $jobCategoryRepository->findAll();
        return $this->render('jobs/jobs.html.twig', [
            'jobs' => $jobs,
            'categories' => $categories

        ]);
    }
    #[Route('/détails/{id}', name: 'détails')]
    public function details(Job $job): Response
    {
        return $this->render('jobs/détails.html.twig', [
            'job' => $job

        ]);
    }
}
