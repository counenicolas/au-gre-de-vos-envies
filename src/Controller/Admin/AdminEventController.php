<?php
namespace App\Controller\Admin;

use App\Repository\BusinessRepository;
use App\Repository\EventRepository;
use App\Repository\WeddingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
    class AdminEventController extends AbstractController
    {

    #[Route(path: '/admin/event/read', name: 'admin_event_read')]
    #[IsGranted('ROLE_ADMIN')]
    public function eventRead(WeddingRepository $weddingRepository, EventRepository $eventRepository, BusinessRepository $businessRepository): Response
    {
    $event = $eventRepository->findAll();
    $wedding = $weddingRepository->findAll();
    $business = $businessRepository->findAll();


    return $this->render('admin/event/read.html.twig', [
        'wedding' => $wedding,
        'event' => $event,
        'business'=> $business,
    ]);

    }
}