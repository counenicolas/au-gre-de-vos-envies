<?php


namespace App\Controller\Admin;

use App\Entity\Job;
use App\Form\JobAddType;
use App\Form\JobEditType;
use App\Repository\JobRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminJobController extends AbstractController
{
    #[Route('/admin/job', name: 'admin_job')]
    public function index(JobRepository $jobRepository): Response
    {
        $jobs = $jobRepository->findAll();

        return $this->render('admin/job/read.html.twig', [
            'jobs' => $jobs,
        ]);
    }

    #[Route('/admin/job/add', name: 'admin_job_add')]
    public function new(Request $request, EntityManagerInterface $em): Response
    {
        $job = new Job();
        $form = $this->createForm(JobAddType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($job);
            $em->flush();

            return $this->redirectToRoute('admin_job');
        }

        return $this->render('admin/job/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/admin/job/{id}/edit', name: 'admin_job_edit')]
    public function edit(Request $request, Job $job, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(JobEditType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            return $this->redirectToRoute('admin_job');
        }

        return $this->render('admin/job/edit.html.twig', [
            'form' => $form->createView(),
            'job' => $job,
        ]);
    }

    #[Route('/admin/job/{id}/delete', name: 'admin_job_delete', methods: ['POST'])]
    public function delete(Request $request, Job $job, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('delete' . $job->getId(), $request->request->get('_token'))) {
            $em->remove($job);
            $em->flush();
        }

        return $this->redirectToRoute('admin_jobs');
    }
}