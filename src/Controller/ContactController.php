<?php

namespace App\Controller;





use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Attribute\Route;

class ContactController extends AbstractController
{

    #[Route('/contact', name: 'contact')]
        public function email(MailerInterface $mailer, Request $request): Response{
            $contact = new Contact();
            $form = $this->createForm(ContactType::class, $contact);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
                $email = (new Email())
                    ->from($contact->getEmail())
                    ->to('admin@admin.com')
                    ->subject($contact->getSubject())
                    ->html('<p>'.$contact->getMessage().'</p>');

                    $mailer->send($email);
                    $this->addFlash('success', 'Votre mail a bien été envoyé');

                return $this->redirectToRoute('home');
            }
            return $this->render('contact/contact.html.twig', [
                'form' => $form,
            ]);
        }
}